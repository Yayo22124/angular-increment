import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'increment';
  i = 0;
  incrementar(){
    this.i += 1;
  }
  decrementar(){
    this.i -= 1;
  }

}
